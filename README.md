# README #

Author: Alyssa Kelley, alyssak@uoregon.edu

A "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### What is this repository for? ###

The objectives of this mini-project are:

  * Experience with GIT workflow with separate configuration: Fork the project, make and test changes locally, commit; turn in configuration file with reference to repo. (Project 0 is practice for this part.) 
  * Extend a tiny web server in Python, to check understanding of basic web architecture
  * Use automated tests to check progress (plus manual tests for good measure)

### What do I need?  Where will it work? ###

* Designed for Unix, mostly interoperable on Linux (Ubuntu) or MacOS. May also work on Windows, but no promises. A Linux virtual machine may work, but our experience has not been good; you may want to test on shared server ix-dev.

* You will need Python version 3.4 or higher. 

* Designed to work in "user mode" (unprivileged), therefore using a port number above 1000 (rather than port 80 that a privileged web server would use)

* Windows 10 note: The new Windows bash on ubuntu looks promising. If you are running Windows 10, please give this a try and let me know if the Ubuntu/bash environment is suitable for CIS 322 develpment. 

### Assignment ###
* This repository is forked from the CIS 322 class bitbucket repo.
* The added functionality is in pageserver.py is: If URL ends with `name.html` or `name.css` (i.e., if `path/to/name.html` is in document path (from DOCROOT)), send content of `name.html` or `name.css` with proper http response, if `name.html` is not in current directory Respond with 404 (not found), and if a page starts with one of the symbols(~ // ..), respond with 403 forbidden error. In this case "name" is refering to any name for the file. For a working example, this would be trivia.html in the pages directory in this repo.

For example, `url=localhost:5000/..name.html` or `/~name.html` would give 403 forbidden error.

For another example, `url=localhost:5000/trivia.html` or `/trivia.html` would be a 200 success.
  

### Testing ###
* This is tested via manual webpage testing, curl testing, and automated testing using the test.sh script in the tests directory in this repo. In all of the steps listed below, port refers to the port you are using from your credentials file. This would be from 5000-8000.

* To do manually testing:
1) run make start
2) open a browser and run localhost:port/filename
3) see the logged information in the terminal, and if this is a valid file, the webpage should be properly displayed
4) run make stop

* To do this with the curl command:
1) run make start
2) open a new terminal window and cd into the tests directory
3) run curl http://localhost:port/filename
4) go back to the server terminal window and run make stop

* To do this with the automated test script:
1) run make start
2) open a new terminal window and cd into the tests directory
3) run ./tests.sh http://localhost:port
4) the results of the test script will display in the terminal window
5) go back to the server terminal window and run make stop

